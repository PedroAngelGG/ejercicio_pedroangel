CREATE DATABASE IF NOT EXISTS `ejercicio_pedroangel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ejercicio_pedroangel`;

CREATE TABLE IF NOT EXISTS `lecturas` (
  `id_medidor` int(10) unsigned NOT NULL,
  `nivel_gas` smallint(6) NOT NULL DEFAULT '0',
  `nivel_bateria` tinyint(3) NOT NULL DEFAULT '0',
  `fecha_lectura` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `FK_lecturas_medidores` (`id_medidor`),
  CONSTRAINT `FK_lecturas_medidores` FOREIGN KEY (`id_medidor`) REFERENCES `medidores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `medidores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `numero` varchar(9) NOT NULL,
  `descripcion` varchar(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  `tipo` enum('MNI','MNA','MNT') NOT NULL,
  `fecha_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_modifico` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Índice 2` (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_lecturas_acciones`(
	IN `_id` INT(11),
	IN `_nivel_gas` SMALLINT(6),
	IN `_nivel_bateria` TINYINT(3)
)
    COMMENT 'agregar lecturas'
MAIN:BEGIN
	IF NOT EXISTS(SELECT id FROM medidores WHERE id=_id) THEN
		SELECT 'Advertencia' respuesta, 'warning' AS alerta,'Advertencia, no se detecto medidor para registrar lectura.' mensaje;
		LEAVE MAIN;
	END IF;
	
	INSERT INTO lecturas SET id_medidor=_id,nivel_gas=_nivel_gas,nivel_bateria=_nivel_bateria;
	IF ROW_COUNT() > 0 THEN
		SELECT 'Exito' respuesta, 'success' AS alerta, 'Registro agregado correctamente.' AS mensaje;
	ELSE
		SELECT 'Error' respuesta, 'error' AS alerta,'Error, Favor de intentar más tarde.' mensaje;
	END IF;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_medidores_acciones`(
	IN `_accion` VARCHAR(20),
	IN `_id` INT,
	IN `_numero` VARCHAR(9),
	IN `_descripcion` VARCHAR(20),
	IN `_version` VARCHAR(20),
	IN `_tipo` ENUM('MNI','MNA','MNT')
)
    COMMENT 'agrega/modifica/elimina medidores'
MAIN:BEGIN
	CASE _accion
		 WHEN  'agregar' THEN
			IF EXISTS(SELECT * FROM medidores WHERE numero = _numero OR descripcion = _descripcion)THEN
				SELECT 'Advertencia' respuesta, 'warning' AS alerta, 'Registro existente, verifique los datos.' AS mensaje;
				LEAVE MAIN;
			END IF;

			IF (_tipo<>'MNI' AND _tipo<>'MNA' AND _tipo<>'MNT') THEN
				SELECT 'Advertencia' respuesta, 'warning' AS alerta, 'Campo tipo solo puede seleccionarse valor de la siguiente lista MNI, MNA ó MNT, verifique los datos.' AS mensaje;
				LEAVE MAIN;
			END IF;

			INSERT INTO medidores SET numero=_numero,descripcion=_descripcion,version=_version,tipo=_tipo;
			IF ROW_COUNT() > 0 THEN
				SELECT 'Exito' respuesta, 'success' AS alerta, 'Registro agregado correctamente.' AS mensaje,LAST_INSERT_ID() AS lastid;
			ELSE
				SELECT 'Error' respuesta, 'error' AS alerta,'Error, Favor de intentar más tarde.' mensaje;
			END IF;
		 WHEN  'modificar' THEN
			IF EXISTS(SELECT * FROM medidores WHERE (numero = _numero OR descripcion = _descripcion) AND id <> _id)THEN
				SELECT 'Advertencia' respuesta, 'warning' AS alerta, 'Registro existente, verifique los datos.' AS mensaje;
				LEAVE MAIN;
			END IF;

			IF (_tipo<>'MNI' AND _tipo<>'MNA' AND _tipo<>'MNT') THEN
				SELECT 'Advertencia' respuesta, 'warning' AS alerta, 'Campo tipo solo puede seleccionarse valor de la siguiente lista MNI, MNA ó MNT, verifique los datos.' AS mensaje;
				LEAVE MAIN;
			END IF;
			
			UPDATE medidores SET numero=_numero,descripcion=_descripcion,version=_version,tipo=_tipo WHERE id=_id;
			IF ROW_COUNT() > 0 THEN
				SELECT 'Exito' respuesta, 'success' AS alerta, 'Registro modificado correctamente.' AS mensaje,_id AS lastid;
			ELSE
				SELECT 'Advertencia' respuesta, 'warning' AS alerta,'No hubo cambios, registro quedó tal cual.' mensaje;
			END IF;
		 WHEN  'eliminar' THEN
			DELETE FROM medidores WHERE id = _id;
			IF ROW_COUNT() > 0 THEN
				SELECT 'Exito' respuesta, 'success' AS alerta, 'Registro eliminado correctamente.' AS  mensaje;
			ELSE
				SELECT 'Error' respuesta, 'error' AS alerta,'Error, Favor de intentar más tarde.' mensaje;
			END IF;
	 END CASE;
END//
DELIMITER ;

CREATE TABLE `vw_lecturas_medidores` (
	`id` INT(10) UNSIGNED NOT NULL,
	`numero` VARCHAR(9) NOT NULL COLLATE 'utf8_general_ci',
	`descripcion` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`version` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`nivel_bateria` INT(4) NOT NULL,
	`estatus` VARCHAR(26) NOT NULL COLLATE 'utf8mb4_general_ci',
	`fecha_instalacion` VARCHAR(19) NOT NULL COLLATE 'utf8mb4_general_ci',
	`tipo` ENUM('MNI','MNA','MNT') NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

DROP TABLE IF EXISTS `vw_lecturas_medidores`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_lecturas_medidores` AS SELECT t1.id, t1.numero, t1.descripcion, t1.version, IFNULL(MIN(t2.nivel_bateria),0) nivel_bateria
, IFNULL(
	IF(t2.fecha_lectura BETWEEN DATE_SUB(NOW(), INTERVAL 24 HOUR) AND NOW(),
		'Actualizado',
		CONCAT(TIMESTAMPDIFF(DAY, t2.fecha_lectura, NOW()),' dÃ­as')
	),
	'No hay lecturas'
) estatus
, IFNULL(MIN(t2.fecha_lectura),'') fecha_instalacion, t1.tipo
FROM medidores t1
LEFT JOIN lecturas t2 ON t2.id_medidor=t1.id
GROUP BY t1.id ;