var registros = [];
var tabla_medidores = $('#tbl_medidores').DataTable({
	language: {
		lengthMenu: "Mostrar _MENU_ entradas",
	    info: "Mostrando _START_ a _END_ de _TOTAL_ entradas", // Mostrando _START_ a _END_ de _TOTAL_ registros
	    infoEmpty : " ", // Mostrando 0 a 0 de 0 registros
	    infoFiltered : " ", // Buscado de _MAX_ registros
	    search : "Buscar:",
	    emptyTable : "No se encontraron registros", // No se encontraron registros
	    zeroRecords : "No se encontraron coincidencias",
	    paginate : {
	        first : "Primero",
	        last : "Ultimo",
	        next : "Siguiente",
	        previous : "Anterior"
	    }
	}
});

// consultar lecturas medidores
function consultarTablaMedidores(){
	var req = $.ajax({
	    method: "POST",
	    url: 'http://localhost/ejercicio_pedroangel/webservice/consultar_lecturas_medidores',
	    data: {}
	});
	req.done(function(data){
	    console.log(data);
	    registros = data.registros;
	    cargarTablaMedidores(data.registros);
	});
	req.fail(function(jqXHR, textStatus, errorThrown) {
		console.log(jqXHR.responseText);
		console.log(textStatus);
		// console.log(errorThrown);
		console.log('Error', 'Se presento error al realizar la petición al WS. '+jqXHR.responseText);
	    swal("Error comunicación", 'Se presento error al realizar la petición al WS. '+jqXHR.responseText, "error");
	});
}

// llena la tabla
function cargarTablaMedidores(registros){
	tabla_medidores.clear().draw();
	$.each(registros, function (index, row) 
	{
		// console.log(index,row)
		var btn_acciones = 
		'<div class="btn-group" role="group">'+
			'<button name="btnAcciones" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">'+
			'Acciones'+
			'</button>'+
			'<ul class="dropdown-menu" aria-labelledby="btnAcciones">'+
				'<li><a name="'+index+'" class="btn dropdown-item agregar_lectura">Agregar lectura</a></li>'+
				'<li><a name="'+index+'" class="btn dropdown-item modificar">Modificar</a></li>'+
				'<li><a name="'+index+'" class="btn dropdown-item eliminar">Eliminar</a></li>'+
			'</ul>'+
		'</div>';
		
		let fecha_instalacion = '';
		if (row['fecha_instalacion'])
			fecha_instalacion = moment(row['fecha_instalacion'],'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');

		tabla_medidores.row.add([
			row['numero'],
			row['descripcion'],
			row['nivel_bateria'],
			row['estatus'],
			fecha_instalacion,
			row['tipo'],
			btn_acciones
		]);
	});
	tabla_medidores.draw();
}

// insertara o modificara la infomacion del medidor
function guardarMedidor(){
	const accion = $('#id').val()=='' ? 'insertar_medidor':'modificar_medidor';

	var req = $.ajax({
	    method: "POST",
	    url: 'http://localhost/ejercicio_pedroangel/webservice/'+accion,
	    data: JSON.stringify({
	    	"id": $('#numero').val(),
	    	"numero": $('#numero').val(),
			"descripcion": $('#descripcion').val(),
			"version": $('#version').val(),
			"tipo": $('#tipo').val()
	    })
	});
	req.done(function(res){
	    console.log(res);
	    swal(res.respuesta, res.mensaje, res.alerta);
	    if (res.respuesta=='Exito'){
	    	consultarTablaMedidores();
	    	limpiar();
	    }
	});
	req.fail(function(jqXHR, textStatus, errorThrown) {
		console.log(jqXHR.responseText);
		console.log(textStatus);
		// console.log(errorThrown);
		console.log('Error', 'Se presento error al realizar la petición al WS. '+jqXHR.responseText);
	    swal("Error comunicación", 'Se presento error al realizar la petición al WS. '+jqXHR.responseText, "error");
	});
}

// eliminar medidor y sus lecturas
function eliminarMedidor(id){
	var previousWindowKeyDown = window.onkeydown;
	swal({
		title: 'Eliminar medidor y sus lecturas.',
		text: "¿Está seguro de esta acción?",
		type: "info",
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: "Cancelar",
		showCancelButton: true,
		closeOnConfirm: false,
		showLoaderOnConfirm: false,
	}, function () {
		window.onkeydown = previousWindowKeyDown;
		var req = $.ajax({
		    method: "POST",
		    url: 'http://localhost/ejercicio_pedroangel/webservice/eliminar_medidor',
		    data: JSON.stringify({
		    	"id": id
		    })
		});
		req.done(function(res){
		    console.log(res);
		    swal(res.respuesta, res.mensaje, res.alerta);
		    if (res.respuesta=='Exito'){
		    	consultarTablaMedidores();
		    	limpiar();
		    }
		});
		req.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR.responseText);
			console.log(textStatus);
			// console.log(errorThrown);
			console.log('Error', 'Se presento error al realizar la petición al WS. '+jqXHR.responseText);
		    swal("Error comunicación", 'Se presento error al realizar la petición al WS. '+jqXHR.responseText, "error");
		});
	});
}

// insertara o modificara la infomacion del medidor
function agregarLectura(){
	var req = $.ajax({
	    method: "POST",
	    url: 'http://localhost/ejercicio_pedroangel/webservice/agregar_lectura',
	    data: JSON.stringify({
	    	"id": $('#id').val(),
	    	"nivel_gas": $('#nivel_gas').val(),
			"nivel_bateria": $('#nivel_bateria').val()
	    })
	});
	req.done(function(res){
	    console.log(res);
	    swal(res.respuesta, res.mensaje, res.alerta);
	    if (res.respuesta=='Exito'){
			$('#lecturasModal').modal('toggle')
	    	consultarTablaMedidores();
	    	limpiar();
	    }
	});
	req.fail(function(jqXHR, textStatus, errorThrown) {
		console.log(jqXHR.responseText);
		console.log(textStatus);
		// console.log(errorThrown);
		console.log('Error', 'Se presento error al realizar la petición al WS. '+jqXHR.responseText);
	    swal("Error comunicación", 'Se presento error al realizar la petición al WS. '+jqXHR.responseText, "error");
	});
}

// limpiamos los campos del formulario
function limpiar(){
	$('.form-control').val('')
}

consultarTablaMedidores();

tabla_medidores.on('click', 'li a.dropdown-item.modificar', function(){
	// const row = $(this).parents('tr');
	const index = $(this).attr('name');
	console.log(registros[index])
	$.each(registros[index], function(key,value){
		$('#'+key).val(value);
	})
    $('html, body').animate({scrollTop:0}, '300'); // regresa al top del modulo
});

tabla_medidores.on('click', 'li a.dropdown-item.eliminar', function(){
	// const row = $(this).parents('tr');
	const index = $(this).attr('name');
	console.log(registros[index])
	eliminarMedidor(registros[index]['id'])
});

tabla_medidores.on('click', 'li a.dropdown-item.agregar_lectura', function(){
	// const row = $(this).parents('tr');
	const index = $(this).attr('name');
	console.log(registros[index])
	$('#id').val(registros[index]['id']);
	$('label[name=numero]').text(registros[index]['numero']);
	$('label[name=descripcion]').text(registros[index]['descripcion']);
	$('#lecturasModal').modal('toggle')
});