<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="dist/bootstrap/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="dist/datatables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert/sweetalert.css"/>

    <title>Ejercicio Pedro Angel</title>
  </head>
  <body>
    <div class="container">
      <h1>Medidores</h1>
      <form name="formMedidores" class="row g-3 needs-validation" novalidates action="javascript:guardarMedidor()">
        <input type="hidden" class="form-control" id="id">
        <div class="col-md-3 col-sm-6">
          <label for="numero" class="form-label">Número</label>
          <input type="text" class="form-control" id="numero" autocomplete="off" required>
        </div>
        <div class="col-md-3 col-sm-6">
          <label for="descripcion" class="form-label">Descripción</label>
          <input type="text" class="form-control" id="descripcion" autocomplete="off" required>
        </div>
        <div class="col-md-3 col-sm-6">
          <label for="version" class="form-label" autocomplete="off">Versión</label>
          <input type="text" class="form-control" id="version">
        </div>
        <div class="col-md-3 col-sm-6">
          <label for="tipo" class="form-label">Tipo</label>
          <select id="tipo" class="form-control" required>
            <option value="">Seleccionar...</option>
            <option value="MNI">MNI</option>
            <option value="MNA">MNA</option>
            <option value="MNT">MNT</option>
          </select>
        </div>
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
          <button class="btn btn-success">Guardar</button>
          <button type="button" class="btn btn-secondary" onclick="limpiar()">Limpiar</button>
        </div>
      </form>

      <hr>
      <table id="tbl_medidores" class="table table-bordered">
        <thead>
          <tr>
            <th>Medidor</th>
            <th>Descripción</th>
            <th>Batería %</th>
            <th>Estatus</th>
            <th>Fecha instalación</th>
            <th>Tipo</th>
            <th>Acciones</th>
          </tr>
        </thead>
      </table>
    </div>

    <div class="modal fade" id="lecturasModal" tabindex="-1" aria-labelledby="lecturasModallLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="lecturasModallLabel">Agregar lectura</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <strong>Medidor: </strong><label name="numero"></label>
            <strong>Descripción: </strong><label name="descripcion"></label>
            <form name="formLecturas" class="row g-3 needs-validation" novalidates action="javascript:agregarLectura()">
              <div class="col-md-6 col-sm-6">
                <label for="numero" class="form-label">Nivel gas</label>
                <input type="number" class="form-control" id="nivel_gas" autocomplete="off" step="1" min="0" required>
              </div>
              <div class="col-md-6 col-sm-6">
                <label for="descripcion" class="form-label">Nivel batería</label>
                <input type="number" class="form-control" id="nivel_bateria" autocomplete="off" step="1" min="0" required>
              </div>              
              <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <button class="btn btn-success">Agregar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap Bundle with Popper -->
    <script type="text/javascript" src="dist/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="dist/jquery/jqueryui.min.js"></script>
    <script src="dist/bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script type="text/javascript" src="dist/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="dist/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="dist/moment/moment.min.js"></script>
    <script type="text/javascript" src="dist/moment/es.js"></script>
    <script src="js/controlador.js"></script>
  </body>
</html>