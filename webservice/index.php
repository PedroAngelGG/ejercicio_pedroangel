<?php
$time_start = microtime(true);
require_once('modelos/Conexion.php');
require_once('modelos/Utilerias.php');
require_once('modelos/Errores.php');
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input'), true); //datos decodificados a JSON

try{
	$conn = new ConexionDB();
	require_once('metodos/'.$_GET['metodo'].'.php');
	//codificamos el JSON
	echo json_encode($result);//imprime respuesta JSON
	// $time_end = microtime(true) - $time_start; 
	// echo "Tiempo = ".$time_end;
}
catch(Errores $e){
    echo json_encode(["respuesta"=>"Fallo validación", "mensaje" => $e->getMessage()]);
	
}
catch(PDOException $e){
    echo json_encode(["respuesta"=>"Fallo servidor (BD)", "mensaje" => $e->getMessage()]);
}
catch (Exception $e){
    echo json_encode(["respuesta"=>"Fallo ejecución", "mensaje" => $e->getMessage()]);
}


?>