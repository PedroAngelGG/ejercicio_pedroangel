<?php

	class Errores extends Exception
	{
		public $objeto = null;

		/*

			REDEFINE LA FUNCION POR LO QUE EL MENSAJE NO ES OPCIONAL

		*/

		public function __construct($message, $code = 0, $objeto = null, Exception $previous = null)
		{
			parent::__construct($message, $code, $previous);
			$this->objeto = $objeto;
		}

		public function __toString()
		{
			return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
		}

		public function obtenerObjeto()
		{
			return $this->objeto;
		}
	}

?>