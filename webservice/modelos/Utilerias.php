<?php
	/**
	* Funciones generales
	* Contiene metodos de ayuda, validaciones y otros.
	*/
	class Utilerias
	{
		/**
		* Validar parameteros
		*
		* @author Pedro Garcia Glez <garciaglez@outlook.com>
		* @param $parametros (array) Parametros del request
		* @return excepcion
		*/
		public static function validarParametros($parametros){
			foreach ($parametros as $key => $row) {
				if (!$row['required']) { // existe o vacio
					throw new Errores("Campo '".$key."' (valor ".$row['desctype'].") faltante o se encuentra sin dato en la petición, favor de verificar.",-1);
				}
				elseif (gettype($row['value'])!=$row['type']) { // tipo de dato
					throw new Errores("Campo '". $key."' su valor debería ser ".$row['desctype'].", favor de verificar.",-2);
				}
			}
		}
	}
?>