<?php
/**
* Conexiones a Base de datos
* contiene metodos para la conexion y ejecucion de comandos para la base de datos.
*/
class ConexionDB
{
	//propiedades
	private $server="localhost";
	private $usuario="root";
	private $password="";
	private $DB="ejercicio_pedroangel";
	/*constructor
	inicializa al ser instaciada la clase
	*/
	public function __construct(){
		//invoca un metodo de la misma clase y parametros de la misma clase
		$this->cargarConexion($this->server, $this->DB, $this->usuario, $this->password);
	}

	/**
	* Carga conexion
	*
	* @author Pedro Garcia Glez <garciaglez@outlook.com>
	* @param $server (string) Host del servidor
	* @param $DB (string) Nombre de la base de datos
	* @param $usuario (string) Nombre del usuario de acceso
	* @param $password (string) Contraseña de acceso
	* @return conexion
	*/
	private function cargarConexion($server, $DB, $usuario, $password){
		$dsn = "mysql:host=" . $server . ";dbname=" . $DB . ";charset=utf8;";
		$opciones = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

		$this->conexion = new PDO($dsn, $usuario, $password, $opciones);
	}

	/**
	* Ejecuta comandos de la Base de Datos (SELECT,INSERT,UPDATE,DELETE)
	*
	* @author Pedro Garcia Glez <garciaglez@outlook.com>
	* @param $query (string) Comando
	* @param $parametros (array) Parametros en el comando
	* @param $tipo_retorno (int) Tipo del resultado 1=regresa 1 registro | 2=regresa 1 o más registros
	* @return $result (array) Lista de registros de la consulta
	*/
	public function Query($query,$parametros, $tipo_retorno=1){
		//Prepara una sentencia para su ejecución y devuelve un objeto sentencia
		$sth = $this->conexion->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)); 
		if(isset($parametros)){
			//recorre el arreglo de los parametros
			foreach ($parametros as $clave => $valor)
			{	
				$sth->bindValue(":$clave", $valor);
			}
		}
		$sth->execute(); //Ejecuta una sentencia preparada
		$result = $tipo_retorno==1 ? $sth->fetch(PDO::FETCH_ASSOC) : $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth->closeCursor(); //Cierra un cursor, habilitando a la sentencia para que sea ejecutada otra vez
		
		return $result;
	}

	public function llamarProcedimiento($procedimiento, $parametros, $tipo_retorno=0) {
		$conexion = $this->conexion;

		$last_key = array_keys($parametros)[count($parametros)-1];
		$query = "CALL ".$procedimiento."(";
		foreach ($parametros as $key => $value) {
			// concatenar nombre de los parametros
			$query.=":".$key;
			if ($key!=$last_key) {
				$query.=",";
			}
		}
		$query.=");";

		$conexion = $conexion->prepare($query);

		foreach ($parametros as $clave => $valor)
		{
			if (is_integer($valor))
				$conexion->bindValue(":$clave", $valor, PDO::PARAM_INT);
			else
				$conexion->bindValue(":$clave", $valor, PDO::PARAM_STR);
		}
		$conexion->execute();

		switch ($tipo_retorno)
		{
			case 1:
				$resultado = $conexion->fetch(PDO::FETCH_ASSOC);
				break;
			case 2:
				$resultado = $conexion->fetchAll(PDO::FETCH_ASSOC);	
				break;
			default:
				$resultado = true;
		}

		$conexion->closeCursor();
		#print_r($resultado);
		return $resultado;
	}
}
?>