<?php

$validar_parametros = [
	'id' => [
		'value' => isset($data['id']) ? intval($data['id']) : 0,
		'required' => isset($data['id']) && intval($data['id'])!=0 ? true : false,
		'type' => 'integer',
		'desctype' => 'numerico',
	],
	'nivel_gas' => [
		'value' => isset($data['nivel_gas']) ? intval($data['nivel_gas']) : 0,
		'required' => isset($data['nivel_gas']) && intval($data['nivel_gas'])!=0 ? true : false,
		'type' => 'integer',
		'desctype' => 'numerico',
	],
	'nivel_bateria' => [
		'value' => isset($data['nivel_bateria']) ? intval($data['nivel_bateria']) : 0,
		'required' => isset($data['nivel_bateria']) && intval($data['nivel_bateria'])!=0 ? true : false,
		'type' => 'integer',
		'desctype' => 'numerico',
	]
];
	
Utilerias::validarParametros($validar_parametros);

// incluir parametros del request
foreach ($validar_parametros as $key => $row) {
	$parametros[$key] = $row['value'];
}
//invocamos un metodo de la clase instanciada
$result = $conn->llamarProcedimiento('sp_lecturas_acciones',$parametros, 1);
?>