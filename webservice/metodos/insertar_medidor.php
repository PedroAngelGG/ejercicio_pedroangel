<?php
$validar_parametros = [
	'numero' => [
		'value' => isset($data['numero']) ? strval($data['numero']) : "",
		'required' => isset($data['numero']) && strval($data['numero'])!="" ? true : false,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	],
	'descripcion' => [
		'value' => isset($data['descripcion']) ? strval($data['descripcion']) : "",
		'required' => isset($data['descripcion']) && strval($data['descripcion'])!="" ? true : false,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	],
	'version' => [
		'value' => isset($data['version']) ? strval($data['version']) : "",
		'required' => true,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	],
	'tipo' => [
		'value' => isset($data['tipo']) ? strval($data['tipo']) : "",
		'required' => isset($data['tipo']) && strval($data['tipo'])!="" ? true : false,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	]
];
	
Utilerias::validarParametros($validar_parametros);

// parametros iniciales
$parametros = ['accion'=>'agregar', 'id'=>''];
// incluir parametros del request
foreach ($validar_parametros as $key => $row) {
	$parametros[$key] = $row['value'];
}
//invocamos un metodo de la clase instanciada
$result = $conn->llamarProcedimiento('sp_medidores_acciones',$parametros, 1);
?>