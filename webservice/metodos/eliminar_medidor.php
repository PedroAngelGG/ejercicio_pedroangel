<?php

$validar_parametros = [
	'id' => [
		'value' => isset($data['id']) ? intval($data['id']) : 0,
		'required' => isset($data['id']) && intval($data['id'])!=0 ? true : false,
		'type' => 'integer',
		'desctype' => 'numerico',
	],
	'numero' => [
		'value' => isset($data['numero']) ? strval($data['numero']) : "",
		'required' => true,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	],
	'descripcion' => [
		'value' => isset($data['descripcion']) ? strval($data['descripcion']) : "",
		'required' => true,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	],
	'version' => [
		'value' => isset($data['version']) ? strval($data['version']) : "",
		'required' => true,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	],
	'tipo' => [
		'value' => isset($data['tipo']) ? strval($data['tipo']) : "",
		'required' => true,
		'type' => 'string',
		'desctype' => 'alfanumerico',
	]
];
	
Utilerias::validarParametros($validar_parametros);

// parametros iniciales
$parametros = ['accion'=>'eliminar'];
// incluir parametros del request
foreach ($validar_parametros as $key => $row) {
	$parametros[$key] = $row['value'];
}
//invocamos un metodo de la clase instanciada
$result = $conn->llamarProcedimiento('sp_medidores_acciones',$parametros, 1);
?>